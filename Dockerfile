FROM node:21-alpine3.18

WORKDIR /app

COPY . /app

RUN npm i

EXPOSE 5173

CMD ["npm","run","dev","--","--host"]